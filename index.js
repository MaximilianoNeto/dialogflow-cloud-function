'use strict';

const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });

  function welcome(agent) {
    agent.add(`Seja bem-vindo ao fulfillment!`);
  }

  // Função da intent fallback
  function fallback(agent) {
    agent.add(`Não entendi o fulfillment`);
  }

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();

  // Aqui eu digo que a intenção no console vai chamar a minha função javascript
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);

  agent.handleRequest(intentMap);
});
